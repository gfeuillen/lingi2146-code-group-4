Ecole Polytechnique de Louvain, UCL  
LINGI2146 Mobile and embedded computing  
Software project for WSNs/IoT  

Group 4, second semester 2016  
Gauthier Feuillen, Maxime de Mol, Lena Peschke  

### Contents ###

* human-detector/:  source code for the human detector mote
* lightswitch-server/:  source code for the light switch/server mote
* LINGI2146-2016-project-group4.pdf:  report


### How to install the code on the motes? ###

#### Required components ####

* 1 Zolertia Z1
* 1 Zolertia RE-Mote
* 1 Phidgets 1113\_0 Mini Joy Stick Sensor (with connection cable)
* 1 Phidgets 1146\_0 IR Reflective Sensor 1-4mm (with connection cable)
* 1 or 2 USB cables
* (optional) 2 AA batteries
* the Contiki VM, available at [Contiki OS](http://www.contiki-os.org/start.html)

#### Steps ####

1. Connect the joystick to the Z1 and the IR sensor to the RE-Mote (ADC3=5V).
2. Compile, Install & Log the human-detector.

  1. Connect the RE-Mote to your PC, launch the Contiki VM and add the RE-Mote as USB device.
  2. Place yourself in the `human-detector/` folder.
  3. `$ make TARGET=zoul savetarget`  
     This step is not mandatory if the `Makefile.target` is present in the directory.
  4. `$ make` to compile the human-detector.
  5. `$ sudo make human-detector.upload` (when the Re-mote is connected and ready to be programmed).
  6. `$ sudo make login` to see the log output.

3. Compile, Install & Log the lightswitch-server.

  1. Connect the Z1 to your PC, launch the Contiki VM and add the remote as USB device.
  2. Place yourself in the `lightswitch-server/` folder.
  3. `$ make TARGET=z1 savetarget`.  
     This step is not mandatory if the `Makefile.target` is present in the directory.
  4. `$ make` to compile the lightswitch-server.
  5. `$ sudo make lightswitch-server.upload` (when the Z1 is connected and ready to be programmed)
  6. `$ sudo make login` to see the log output.


4. With this setup, you can now start testing! For more details about the system, please refer to the report.

#### Troubleshooting ####

You might run into some problems when trying to upload the code on the motes or see their logs.

* If there is a problem at the compilation step, you might need to install additional plugins for the compiler. A quick Google search should do the trick.
* If you have trouble uploading code on the motes:

  1. Make sure you can use the `sudo` command. In the Contiki VM, the password is usually set to "user".
  2. Verify that the motes are connected to your PC and that they have been added as USB devices (under the devices option menu of Virtualbox).
  3. The above (2) should resolve any problem concerning the Z1. For the RE-Mote, try to remove the Z1 from the USB devices. If this does not work, try other physical USB ports until it does. You might also need to try it several times by resetting the mote and then trying to upload the code (from our experience: this part is really non deterministic, half a second might change it all).

* Concerning problems when trying to log the motes, redo the steps to upload the code but do not reset the motes continuously.
