/******************************************************************************
 * Ecole Polytechnique de Louvain, UCL
 * LINGI2146 Mobile and Embedded Computing
 * Group: 4
 * Authors: Gauthier Feuillen, Maxime de Mol, Lena Peschke
 * Date: 10/06/2016
 *
 * Human detector mote.
 *
 * Detects people entering and leaving the room
 * and sends notifications to the light switch.
 *
 * Based on Contiki's udp-client.
 *
 ******************************************************************************/

#include <stdbool.h>
#include <string.h>
#include "contiki-lib.h"
#include "contiki-net.h"
#include "contiki.h"
#include "dev/adc-sensors.h"
#include "dev/button-sensor.h"
#include "dev/leds.h"
#include "dev/zoul-sensors.h"
#include "net/ip/resolv.h"
#define DEBUG DEBUG_PRINT
#include "net/ip/uip-debug.h"
#include "messages.h"

#define MAX_PAYLOAD_LEN 40

#define SENSOR_INTERVAL 0.01 * CLOCK_SECOND // interval between IR sensor polls
#define SENSOR_THRESHOLD 2000 // calibration for the IR sensor


static struct uip_udp_conn *client_conn;
static char buf[MAX_PAYLOAD_LEN];

/*----------------------------------------------------------------------------*/
PROCESS(human_detector_process, "HUMAN DETECTOR PROCESS");
AUTOSTART_PROCESSES(&resolv_process, &human_detector_process);
/*----------------------------------------------------------------------------*/


/*
 * Send an UDP message to the server.
 */
static void send_to_server(char *message)
{
    sprintf(buf, message);
    
    PRINTF("HD  send to: ");
    PRINT6ADDR(&client_conn->ripaddr);
    PRINTF("HD  (msg: %s)\n", buf);
    
#if SEND_TOO_LARGE_PACKET_TO_TEST_FRAGMENTATION
    uip_udp_packet_send(client_conn, buf, UIP_APPDATA_SIZE);
#else  /* SEND_TOO_LARGE_PACKET_TO_TEST_FRAGMENTATION */
    uip_udp_packet_send(client_conn, buf, strlen(buf));
#endif /* SEND_TOO_LARGE_PACKET_TO_TEST_FRAGMENTATION */
}

/*
 * Detect when someone enters or leaves the room
 * and send a notification to the server.
 * Switch on/off the LEDs for visual support.
 */
static void detect_human(void)
{
    static int count_entering; // keep a counter of the people in the room
    char message[MAX_PAYLOAD_LEN];
    
    if (count_entering == 0) {
        sprintf(message, HUMAN_IN, ++count_entering);
		leds_on(LEDS_ALL);
    } else if (count_entering == 1) {
        sprintf(message, HUMAN_OUT, --count_entering);
		leds_off(LEDS_ALL);
    } else {
        // should not happen
        PRINTF("HD  ERROR in detect_human\n");
        sprintf(message, "Don't know what's happening: human count is %d ",
                count_entering);
    }
	PRINTF("HD  human count: %d\n", count_entering);
    
    send_to_server(message);
}

/*
 * Read the IR sensor and act on the returned value:
 * if a human is detected, update the state and notify the server.
 */
static void handle_sensor_value(int value)
{
    static int passed_flag;

    if (value < SENSOR_THRESHOLD && passed_flag == 0) {
        detect_human();
        passed_flag = 1; // hold the sensor
    } else if (value >= SENSOR_THRESHOLD && passed_flag == 1) {
        passed_flag = 0; // release the sensor
    } else {
        // should not happen
        //PRINTF("HD  ERROR in handle_sensor_value\n");
    }
}

/*
 * Contiki code.
 * Print the local network addresses of the mote.
 */
static void print_local_addresses(void)
{
    int i;
    uint8_t state;
    
    PRINTF("HD  IPv6 addresses: ");
    for (i = 0; i < UIP_DS6_ADDR_NB; i++) {
        state = uip_ds6_if.addr_list[i].state;
        if (uip_ds6_if.addr_list[i].isused &&
            (state == ADDR_TENTATIVE || state == ADDR_PREFERRED)) {
            PRINT6ADDR(&uip_ds6_if.addr_list[i].ipaddr);
            PRINTF("\n");
        }
    }
}

#if UIP_CONF_ROUTER
/*
 * Contiki code.
 * Set the network addresses for this mote.
 */
static void set_global_address(void)
{
    uip_ipaddr_t ipaddr;
    
    uip_ip6addr(&ipaddr, UIP_DS6_DEFAULT_PREFIX, 0, 0, 0, 0, 0, 0, 0);
    uip_ds6_set_addr_iid(&ipaddr, &uip_lladdr);
    uip_ds6_addr_add(&ipaddr, 0, ADDR_AUTOCONF);
}
#endif /* UIP_CONF_ROUTER */

/*
 * Contiki code.
 * Set up the network connection to the server.
 */
static resolv_status_t set_connection_address(uip_ipaddr_t *ipaddr)
{
#ifndef UDP_CONNECTION_ADDR
#if RESOLV_CONF_SUPPORTS_MDNS
#define UDP_CONNECTION_ADDR       contiki-udp-server.local
#elif UIP_CONF_ROUTER
#define UDP_CONNECTION_ADDR       fd00:0:0:0:0212:7404:0004:0404
#else
#define UDP_CONNECTION_ADDR       fe80:0:0:0:6466:6666:6666:6666
#endif
#endif /* !UDP_CONNECTION_ADDR */
    
#define _QUOTEME(x) #x
#define QUOTEME(x) _QUOTEME(x)
    
    resolv_status_t status = RESOLV_STATUS_ERROR;
    
    if (uiplib_ipaddrconv(QUOTEME(UDP_CONNECTION_ADDR), ipaddr) == 0) {
        uip_ipaddr_t *resolved_addr = NULL;
        status = resolv_lookup(QUOTEME(UDP_CONNECTION_ADDR), &resolved_addr);
        if (status == RESOLV_STATUS_UNCACHED || status == RESOLV_STATUS_EXPIRED) {
            PRINTF("HD  Attempting to look up %s\n", QUOTEME(UDP_CONNECTION_ADDR));
            resolv_query(QUOTEME(UDP_CONNECTION_ADDR));
            status = RESOLV_STATUS_RESOLVING;
        } else if (status == RESOLV_STATUS_CACHED && resolved_addr != NULL) {
            PRINTF("HD  Lookup of \"%s\" succeded!\n", QUOTEME(UDP_CONNECTION_ADDR));
        } else if (status == RESOLV_STATUS_RESOLVING) {
            PRINTF("HD  Still looking up \"%s\"...\n", QUOTEME(UDP_CONNECTION_ADDR));
        } else {
            PRINTF("HD  Lookup of \"%s\" failed. status = %d\n",
                   QUOTEME(UDP_CONNECTION_ADDR), status);
        }
        if (resolved_addr)
            uip_ipaddr_copy(ipaddr, resolved_addr);
    } else {
        status = RESOLV_STATUS_CACHED;
    }
    
    return status;
}

/*----------------------------------------------------------------------------*/
PROCESS_THREAD(human_detector_process, ev, data)
{
    static struct etimer sensor_timer; // timer for the IR sensor
    SENSORS_ACTIVATE(button_sensor);
    adc_zoul.configure(SENSORS_HW_INIT, ZOUL_SENSORS_ADC_ALL);
    
    uip_ipaddr_t ipaddr; // this mote's IP address
    
    PROCESS_BEGIN();
    PRINTF("HD  process started\n");
    
    /* Set the network addresses. */
#if UIP_CONF_ROUTER
    set_global_address();
#endif
    
    print_local_addresses();
    
    static resolv_status_t status = RESOLV_STATUS_UNCACHED;
    while (status != RESOLV_STATUS_CACHED) {
        status = set_connection_address(&ipaddr);
        
        if (status == RESOLV_STATUS_RESOLVING) {
            PROCESS_WAIT_EVENT_UNTIL(ev == resolv_event_found);
        } else if (status != RESOLV_STATUS_CACHED) {
            PRINTF("HD  Can't get connection address.\n");
            PROCESS_YIELD();
        }
    }
    
    /* New UDP connection with the light switch mote. */
    client_conn = udp_new(&ipaddr, UIP_HTONS(3000), NULL);
    udp_bind(client_conn, UIP_HTONS(3001));
    
    PRINTF("HD  created a connection with the light switch ");
    PRINT6ADDR(&client_conn->ripaddr);
    PRINTF(" local/remote port %u/%u\n", UIP_HTONS(client_conn->lport),
           UIP_HTONS(client_conn->rport));
    
    /* Set a timer to periodically poll the IR sensor. */
    etimer_set(&sensor_timer, SENSOR_INTERVAL);
    send_to_server("Connection established");
    
    /* Each time the timer expires, retrieve the IR value and handle it. */
    while (1) {
        PROCESS_YIELD();
        if (etimer_expired(&sensor_timer)) {
            int sensor_value = adc_zoul.value(ZOUL_SENSORS_ADC3);
            handle_sensor_value(sensor_value);    
            etimer_restart(&sensor_timer); // restart the timer
        }
    }
    
    PROCESS_END();
}
/*---------------------------------------------------------------------------*/
