/******************************************************************************
 * Ecole Polytechnique de Louvain, UCL
 * LINGI2146 Mobile and Embedded Computing
 * Group: 4
 * Authors: Gauthier Feuillen, Maxime de Mol, Lena Peschke
 * Date: 10/06/2016
 *
 * Messages exchanged in the network.
 *
 ******************************************************************************/

#ifndef messages_h
#define messages_h

/*
 * Human detector -> light switch
 */
#define HUMAN_IN	"human in"
#define HUMAN_OUT	"human out"

/*
 * Light switch <-> light switch
 */
// no messages, same mote

#endif /* messages_h */
