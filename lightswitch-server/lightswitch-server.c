/******************************************************************************
 * Ecole Polytechnique de Louvain, UCL
 * LINGI2146 Mobile and Embedded Computing
 * Group: 4
 * Authors: Gauthier Feuillen, Maxime de Mol, Lena Peschke
 * Date: 10/06/2016
 *
 * Light switch and server mote.
 *
 * Light switch:
 *  get notified of people entering and leaving the room,
 *  controls the light intensity,
 *  reports the light settings to the server.
 *
 * Server:
 *  receives regular reports from the light switch,
 *  computes a timeline (shedule of light intensities)
 *  and sends it to the light switch.
 *
 * Based on Contiki's udp-server.
 *
 ******************************************************************************/

#include <stdio.h>
#include <string.h>
#include "contiki.h"
#include "contiki-lib.h"
#include "contiki-net.h"
#include "dev/leds.h"
#include "dev/button-sensor.h"
#include "dev/z1-phidgets.h"
#define DEBUG DEBUG_PRINT
#include "net/ip/uip-debug.h"
#include "messages.h"

#define UIP_IP_BUF   ((struct uip_ip_hdr *)&uip_buf[UIP_LLH_LEN])

#define SENSOR_INTERVAL CLOCK_SECOND/5 // interval between joystick sensor polls

#define HOURS 4 // number of hours in one day
#define HOUR 5*CLOCK_SECOND // number of seconds in one hour


/* Light switch */
static struct uip_udp_conn *server_conn;
static char observed_tl[HOURS]; // observed timeline
static char current_intensity = 0;
static int human_count = 0;
static int current_hour = 0;
static char rst = 1; // dirty bit for the joystick

/* Server */
static char computed_tl[HOURS]; // computed timeline

/* Server/light switch */
static char new_observation = 0; // dirty bit for the observed timeline
static struct etimer server_process_timer; // timers to switch processes


/*----------------------------------------------------------------------------*/
PROCESS(switch_process, "SWITCH PROCESS");
PROCESS(server_process, "SERVER PROCESS");
AUTOSTART_PROCESSES(&resolv_process, &switch_process, &server_process);
/*----------------------------------------------------------------------------*/


/*
 * Clear a table of chars.
 */
void initialise_tab(char* tab, int size)
{
    int i;
    for (i = 0; i < size; i++) {
        tab[i] = 0;
    }
}

/*
 * Print a timeline.
 */
void print_ints(char* tab)
{
    int i=0;
    printf(" ");
    for (i = 0; i < HOURS; i++){
        printf("%d ",tab[i]);
    }
    printf("\n");
}


/*
 * Server
 * Compute the timeline based on the recent observations from the light switch.
 * Algorithm: reproduce the observation from the previous day.
 */
static void compute_timeline(void)
{
    int i;
    if (new_observation == 1) {
        // only recompute if fresh data
        for (i = 0; i < HOURS; i++) {
            computed_tl[i] = observed_tl[i];
        }
        new_observation = 0; // wait for the next data
    } else {
        // keep previous computed timeline
    }
}


/*
 * Light switch
 * If someone is in the room, set the light according to the timeline.
 * If no one is in the room, turn off the light.
 */
static void switch_light(void)
{
    int someone_in = human_count >= 1;
    if (someone_in) {
        leds_off(LEDS_ALL);
        leds_on(computed_tl[current_hour]);
        current_intensity = computed_tl[current_hour]; // remember the setting
        PRINTF("LS  switch_light, someone is in\n");
    } else {
        leds_off(LEDS_ALL);
        current_intensity = 0; // remember the setting
        PRINTF("LS  switch_light, no one is in\n");
    }
    PRINTF("LS  switch_light, intensity: %d\n", current_intensity);
}

/*
 * Light switch
 * Get notifications from the human detector about people entering or leaving.
 * Set the light accordingly.
 */
static void tcpip_handler(void)
{   
    if (uip_newdata()) {
        // get the content of the message
        ((char *)uip_appdata)[uip_datalen()] = 0;
        char * data = (char *)uip_appdata;
        PRINTF("LS  message received: %s\n", data);
        
        // detect if someone in or out
        if (strcmp(data, HUMAN_IN)==0) {
            human_count++;
        } else if(strcmp(data, HUMAN_OUT)==0) {
            human_count--;
        }
        PRINTF("LS  human count: %d\n", human_count);
        
        // switch the light (after setting the shared variables)
        switch_light();
        
        // make space for new messages
        memset(&server_conn->ripaddr, 0, sizeof(server_conn->ripaddr));
    }
}

/*
 * Light switch
 * Interpret the joystick input and set the light.
 * Update the observations.
 */
static void change_intensity(int value)
{
    new_observation = 1; // remember that there is new data
    
    if (value >= 2000 && rst == 1) { // switch up, not yet released
        switch (current_intensity) { // set the light +1 if possible
            case 0:
                current_intensity = LEDS_BLUE;
                break;
            case LEDS_BLUE:
                current_intensity = LEDS_GREEN;
                break;
            case LEDS_GREEN:
                current_intensity = LEDS_RED;
                break;
            default:
                break;
        }
        rst = 0;
        PRINTF("LS  change_intensity, new intensity: %d\n", current_intensity);
    } else if (value <= 1850 && rst == 1) { // switch down, not yet released
        switch (current_intensity) { // set the light -1 if possible
            case LEDS_RED:
                current_intensity = LEDS_GREEN;
                break;
            case LEDS_GREEN:
                current_intensity = LEDS_BLUE;
                break;
            case LEDS_BLUE:
                current_intensity = 0;
                break;
            default:
                break;
        }
        rst = 0;
        PRINTF("LS  change_intensity, new intensity: %d\n", current_intensity);
    } else if (value >= 1851 && value <= 1999) { // switch released
        rst = 1;
    } else {
        // should not happen
        //PRINTF("LS  ERROR in change_intensity\n");
    }
    
    leds_off(LEDS_ALL);
    leds_on(current_intensity); // set the LEDs to the chosen value
    observed_tl[current_hour] = current_intensity; // record the change
}

/*
 * Light switch
 * Contiki code.
 * Print the local network addresses of the mote.
 */
static void print_local_addresses(void)
{
    int i;
    uint8_t state;
    
    PRINTF("LS  IPv6 addresses: ");
    for(i = 0; i < UIP_DS6_ADDR_NB; i++) {
        state = uip_ds6_if.addr_list[i].state;
        if(uip_ds6_if.addr_list[i].isused &&
           (state == ADDR_TENTATIVE || state == ADDR_PREFERRED)) {
            PRINT6ADDR(&uip_ds6_if.addr_list[i].ipaddr);
            PRINTF("\n");
        }
    }
}


/*----------------------------------------------------------------------------*/
PROCESS_THREAD(switch_process, ev, data)
{
    static struct etimer et; // timer for the joystick
    SENSORS_ACTIVATE(phidgets);
    static struct etimer hour_timer; // timer to count one "hour"
    
#if UIP_CONF_ROUTER
    uip_ipaddr_t ipaddr;
#endif /* UIP_CONF_ROUTER */
    
    PROCESS_BEGIN();
    PRINTF("LS  light switch started\n");
    
    /* Set up the network addresses. */
#if RESOLV_CONF_SUPPORTS_MDNS
    resolv_set_hostname("contiki-udp-server");
#endif
    
#if UIP_CONF_ROUTER
    uip_ip6addr(&ipaddr, 0xaaaa, 0, 0, 0, 0, 0, 0, 0);
    uip_ds6_set_addr_iid(&ipaddr, &uip_lladdr);
    uip_ds6_addr_add(&ipaddr, 0, ADDR_AUTOCONF);
#endif /* UIP_CONF_ROUTER */
    
    print_local_addresses();
    
    /* Create a connection to the human detector. */
    server_conn = udp_new(NULL, UIP_HTONS(3001), NULL);
    udp_bind(server_conn, UIP_HTONS(3000));
    
    /* Set all timers. */
    etimer_set(&et,SENSOR_INTERVAL);
    etimer_set(&hour_timer,HOUR);
    
    initialise_tab(observed_tl, HOURS); // reset the observed timeline
    
    /* Each time a message comes in, handle it
     *  (update human count, switch light, remember setting).
     * After one hour, set the light according to the computed timeline.
     * Periodically check the joystick and act on input.
     */
    while (1) {
        PROCESS_YIELD();
        if (ev == tcpip_event) {
            // get message
            tcpip_handler();
        } else if (etimer_expired(&hour_timer)) {
            // move on to the next hour
            current_hour = (current_hour+1)%HOURS;
            etimer_reset(&hour_timer);
            printf("LS  hour of the day: %d\n", current_hour);
            if (current_hour == 0) {
                // on each new day, output the timelines
                PRINTF("LS  computed tl --> ");
                print_ints(computed_tl);
                PRINTF("LS  observed tl --> ");
                print_ints(observed_tl);
            }
            switch_light();
        } else if (etimer_expired(&et)) {
            // check switch (joystick) activity
            int val = phidgets.value(PHIDGET3V_1);
            if (human_count > 0) {
                // only handle switch if someone is in the room
                change_intensity(val);
            }
            etimer_reset(&et);
        }
    }
    
    PROCESS_END();
}

PROCESS_THREAD(server_process, ev, data)
{
    PROCESS_BEGIN();
    PRINTF("SV  server started\n");
    
    etimer_set(&server_process_timer,HOURS*HOUR); // set the timer to once a day
    
    initialise_tab(computed_tl, HOURS); // reset the computed timeline
    
    /* Look for a new observed timeline and re-compute the computed timeline. */
    while (1) {
        PROCESS_WAIT_EVENT_UNTIL(etimer_expired(&server_process_timer));
        compute_timeline();
        etimer_reset(&server_process_timer);
        
        PRINTF("SV  computed tl --> ");
        print_ints(computed_tl);
        PRINTF("SV  observed tl --> ");
        print_ints(observed_tl);
    }
    
    PROCESS_END();
}
/*----------------------------------------------------------------------------*/
